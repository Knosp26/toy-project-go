package main

import (
	"fmt"
	"html/template"
	"image"
	"image/color"
	"image/png"
	"math/rand"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"time"
	"github.com/gorilla/mux"
)

func main() {
	router := mux.NewRouter()
	router.PathPrefix("/images/").Handler(http.StripPrefix("/images/", http.FileServer(http.Dir("images"))))
	router.PathPrefix("/_/").Handler(http.StripPrefix("/_/", http.FileServer(http.Dir("_"))))
	router.HandleFunc("/", home)
	router.HandleFunc("/view/{path}", view)
	router.HandleFunc("/api/{sign}", api)
	router.HandleFunc("/generate", genImg).Methods("GET")
	http.ListenAndServe(":8000", router)
}

func home(w http.ResponseWriter, router *http.Request) {
	files, isError := filepath.Glob(filepath.Join("images/", "*"))
	if isError != nil {
		panic(isError)
	}

	fileCount := len(files)
	tmp := template.Must(template.ParseFiles("templates/index.html"))

	isError = tmp.Execute(w, fileCount)
	if isError != nil {
		http.Error(w, isError.Error(), http.StatusInternalServerError)
		return
	}
}

// viewer \! 24.03.30
func view(w http.ResponseWriter, router *http.Request) {
	slug := mux.Vars(router)
	path := slug["path"]
	print(path)
}

// api \! 24.03.30
func api(w http.ResponseWriter, router *http.Request) {
	slug := mux.Vars(router)
	sign := slug["sign"]
	print(sign)
}

func genImg(w http.ResponseWriter, router *http.Request) {
	/*
	지우지 말 것. _______________________________________________________
	tmp := template.Must(template.ParseFiles("templates/generate.html"))
	isError := tmp.Execute(w, nil)
	if isError != nil {
		http.Error(w, isError.Error(), http.StatusInternalServerError)
		return
	} __________________________________________________________________
	*/

	ajax_pixel, isError := strconv.Atoi(router.URL.Query().Get("pixel"))
	if isError != nil {
		panic(isError)
	}

	flow := 0

	pixel := 16
	if ajax_pixel != 0 {
		pixel = ajax_pixel
	}

	wh := pixel * pixel
	if wh <= 1 {
		panic("width and height must be same")
	}

	tileSize := pixel

	img := image.NewRGBA(image.Rect(0, 0, wh, wh))

	for x:=0; x<pixel; x++ {
		for y:=0; y<pixel; y++ {
			flow ++
			v_r := uint8(rand.Intn(256))
			v_g := uint8(rand.Intn(256))
			v_b := uint8(rand.Intn(256))
			v_a := uint8(255)
			colored(
				img, 
				tileSize,
				x*tileSize, 
				y*tileSize,
				color.RGBA{
					R: v_r, 
					G: v_g, 
					B: v_b, 
					A: v_a,
				},
			)
		}
	}

	file := fmt.Sprintf("images/%s_%dx%d.png", time.Now().Format("20060102150405"), pixel, pixel)

	pngF, isError := os.Create(file)
	if isError != nil {
		panic(isError)
	}
	defer pngF.Close()

	isError = png.Encode(pngF, img)
	if isError != nil {
		panic(isError)
	}

	fmt.Println("Successful:", flow)
	defer fmt.Fprintf(w, "/%s", file)
}

func colored(img *image.RGBA, wh, x, y int, color color.RGBA) {
	for newX:=x; newX<x+wh; newX++ {
		for newY:=y; newY<y+wh; newY++ {
			img.Set(newX, newY, color)
		}
	}
}